package com.example.cadastro.front

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.isDigitsOnly
import androidx.navigation.NavController
import com.example.cadastro.R
import com.example.cadastro.back.Validator
import com.example.cadastro.back.sendMail

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Confirmacao(navController: NavController, email:String, codigo:String){
    var mail by remember { mutableStateOf(email)}
    var codigo by remember { mutableStateOf(codigo)}
    var codigoConfirmacao by remember{ mutableStateOf("") }
    var isValidCode by remember { mutableStateOf(false) }
    val context = LocalContext.current

    Column(
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.8f)
            .padding(horizontal = 15.dp, vertical = 50.dp),
            verticalArrangement = Arrangement.Top)

    {// Título
        Row(Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly)

        {
            Image(painter = painterResource(id = R.drawable._8018_200),
                modifier = Modifier.width(50.dp) ,
                contentDescription = "Verify Icon" )

            Text(text = "Confirmação do Email",
                fontWeight = FontWeight.W900,
                fontSize = 26.sp)}
        Spacer(modifier = Modifier.height(200.dp))

        //Label codigo
        Text(text = "Digite o código enviado para:", fontWeight = FontWeight.Bold, fontSize = 22.sp)
        Spacer(modifier = Modifier.height(15.dp))

        //Input codigo
        TextField(value = codigoConfirmacao ,
            onValueChange = {if(it.isDigitsOnly()){ codigoConfirmacao=it}},
            label ={ Text(text = "Código")},
            keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
            modifier = Modifier.fillMaxWidth(),
            singleLine = true)
        Spacer(modifier = Modifier.height(25.dp))



        Spacer(modifier = Modifier.width(5.dp))
        Button(onClick = { codigo = sendMail(mail) }) {
            Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.atualizar),
                contentDescription ="reenviar icon" )
            Spacer(modifier = Modifier.width(5.dp))
            Text(text = "Reenviar código",
                fontSize = 20.sp,
                color = Color.Black
                )
        }


        //Mensagem de sucesso na captura de dados
        if(isValidCode){
            Spacer(modifier = Modifier.height(50.dp))
            Text(text = "Dados capturados com sucesso! Professor já pode dar um 10",
                fontSize = 20.sp)
        }
    }

    //Bloco de botoes
    Column(
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(1f)
            .padding(horizontal = 15.dp, vertical = 20.dp),
            verticalArrangement = Arrangement.Bottom)
    {
        Row(Modifier.fillMaxWidth()){
            Button(onClick = {navController.navigate("Contato")},
                modifier = Modifier.fillMaxWidth(.5f))
            { Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.esquerda),
                contentDescription ="Seta esquerda icon" )}
            Spacer(modifier = Modifier.width(5.dp))

            Button(onClick = {
                isValidCode = Validator(codigoConfirmacao).code(codigo)
                if (!isValidCode){
                    Toast.makeText(context,"Código inválido",Toast.LENGTH_SHORT).show()
                }
                             },
                modifier = Modifier.fillMaxWidth(1f))
            { Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.check),
                contentDescription ="validar icon" )}
        }
    }
}