package com.example.cadastro.front

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.isDigitsOnly
import androidx.navigation.NavController
import com.example.cadastro.R
import com.example.cadastro.back.Validator
import com.example.cadastro.back.sendMail

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Contato (navController: NavController){

    //Variáveis de dados
    var telefone by remember { mutableStateOf("") }
    var email by remember { mutableStateOf("") }

    //Variáveis de controle
    var isValidTel by remember { mutableStateOf(false) }
    var isValidEmail by remember { mutableStateOf(false) }

    Column (
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.8F)
            .padding(horizontal = 15.dp, vertical = 50.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top)
    {

        //Título
        Row(
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Image(
                painter = painterResource(id = R.drawable._8018_200),
                modifier = Modifier.width(50.dp),
                contentDescription = "Verify Icon"
            )
            Spacer(modifier = Modifier.width(20.dp))
            Text(text = "Contato",
                fontWeight = FontWeight.W900,
                fontSize = 26.sp)
        }
        Spacer(modifier = Modifier.height(200.dp))

        //Bloco de Inputs
        Row(Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically)

        {
            Image(
                painter = painterResource(id = R.drawable.telefone),
                modifier = Modifier.width(50.dp),
                contentDescription = "Telefone Icon")
            TextField(
                value = telefone,
                onValueChange = { if (it.isDigitsOnly()) { telefone = it } },
                label = { Text(text = "Telefone") },
                singleLine = true,
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number)
            )
        }
        Spacer(modifier = Modifier.height(20.dp))

        Row(Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically)

        {
            Image(
                painter = painterResource(id = R.drawable.o_email),
                modifier = Modifier.width(50.dp),
                contentDescription = "Email Icon")
            TextField(
                value = email,
                onValueChange = { email = it.replace(" ", "").lowercase() },
                label = { Text(text = "Email") },
                singleLine = true,
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Email)
            )
        }

    }

    //Bloco de botões
    Column (
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(1F)
            .padding(horizontal = 15.dp, vertical = 20.dp),
            verticalArrangement = Arrangement.Bottom)
    {

        Row (Modifier.fillMaxWidth()){

            Button(onClick = {
                navController.navigate("Endereco")
            },
                modifier = Modifier.fillMaxWidth(.3F)
            )
            { Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.esquerda),
                contentDescription ="Seta esquerda icon" )}
            Spacer(modifier = Modifier.width(5.dp))

            Button(onClick = {
                telefone=""
                email=""
            },
                modifier = Modifier.fillMaxWidth(.5F),
                colors = ButtonDefaults.buttonColors(colorResource(id = R.color.test))
            ) { Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.lixeira),
                contentDescription ="lixeira icon" )}
            Spacer(modifier = Modifier.width(5.dp))

            Button(onClick = {
                isValidTel = Validator(telefone).telefone()
                isValidEmail = Validator(email).email()
                if (isValidEmail && isValidTel){
                    val code = sendMail(email)

                    navController.navigate("Confirmacao/$email/$code") //TODO MUDAR CODIGO
                }else{
                    //TODO TOAST
                }
            },
                modifier = Modifier.fillMaxWidth(1F)
            ){Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.direita),
                contentDescription ="Seta direita icon" )}
        }
    }
}