//TODO estilizacao
package com.example.cadastro.front

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.isDigitsOnly
import androidx.navigation.NavController
import com.example.cadastro.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.net.URL

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Endereco(navController: NavController){

    //Bloco de variáveis de dados
    var cep by remember { mutableStateOf("")}
    var endereco by remember { mutableStateOf("aa")}
    var numero by remember { mutableStateOf("")}
    var uf by remember { mutableStateOf("")}
    var cidade by remember { mutableStateOf("")}
    var bairro by remember { mutableStateOf("")}
    var logradouro by remember { mutableStateOf("")}
    var complemento by remember { mutableStateOf("")}




    Column (
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.8f)
            .padding(horizontal = 15.dp, vertical = 50.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top)

    {
        //Título
        Row(
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center)

        {
            Image(
                painter = painterResource(id = R.drawable.mapas_e_bandeiras),
                modifier = Modifier.width(50.dp),
                contentDescription = "Verify Icon"
            )
            Spacer(modifier = Modifier.width(20.dp))

            Text(text = "Endereço",
                fontWeight = FontWeight.W900,
                fontSize = 26.sp)
        }
        Spacer(modifier = Modifier.height(150.dp))

        //Blocos de inputs

        Row(    //CEP+BUTTON
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start)
        {
            TextField(value = cep,
                onValueChange = { if (it.isDigitsOnly()) { cep = it }},
                label = { Text(text = "CEP") },
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                singleLine = true
            )
            Spacer(modifier = Modifier.width(12.dp))

            Button(onClick = { cepAPI(cep) { resultado -> endereco = resultado }},
                modifier = Modifier
                    .height(55.dp)
                    .width(80.dp), shape = RoundedCornerShape(5.dp), enabled = cep.length == 8)
            {
                Image(
                    painter = painterResource(id = R.drawable.atualizar),
                    contentDescription = "reload icon",
                    modifier = Modifier.width(25.dp)
                )
            }
        }
        Spacer(modifier = Modifier.height(10.dp))

        Row( //CIDADE+ UF
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start)

        {
            TextField(
                value = cidade,
                onValueChange = { cidade = it },
                label = { Text(text = "Cidade") },
                singleLine = true)
            Spacer(modifier = Modifier.width(8.dp))

            TextField(value = uf,
                onValueChange = { uf = it },
                label = { Text(text = "UF") })
        }
        Spacer(modifier = Modifier.height(10.dp))

        TextField(
            value = bairro,
            onValueChange = { bairro = it },
            label = { Text(text = "Bairro") },
            singleLine = true)
        Spacer(modifier = Modifier.height(10.dp))

        TextField(
            value = logradouro,
            onValueChange = { logradouro = it },
            label = { Text(text = "Logradouro") },
            singleLine = true
        )
        Spacer(modifier = Modifier.height(10.dp))

        Row(Modifier.fillMaxWidth()) { //  NUMERO + COMPLEMENTO
            TextField(
                value = numero,
                onValueChange = { numero = it },
                label = { Text(text = "Numero") },
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                modifier = Modifier.fillMaxWidth(.6f),
                singleLine = true)
            Spacer(modifier = Modifier.width(10.dp))

            TextField(
                value = complemento,
                onValueChange = { complemento = it },
                label = { Text(text = "Comp") },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true)
        }
    }

    //Bloco de botões
    Column (
        Modifier
            .fillMaxHeight(1f)
            .fillMaxHeight(1f)
            .padding(horizontal = 15.dp, vertical = 20.dp),
        verticalArrangement = Arrangement.Bottom)

    {
        Row (Modifier.fillMaxWidth()){
            Button(onClick = {
                navController.navigate("DadosPessoais")
            },
                modifier = Modifier.fillMaxWidth(0.3f)
                ) { Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.esquerda),
                contentDescription ="Seta esquerda icon" )}
            Spacer(modifier = Modifier.width(5.dp))

            Button(onClick = {
                cep=""
                logradouro =""
                numero=""
                complemento=""
                uf=""
                bairro=""
                cidade=""
            },
               modifier = Modifier.fillMaxWidth(0.5f),
                colors = ButtonDefaults.buttonColors(colorResource(id = R.color.test))
                ){ Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.lixeira),
                contentDescription ="lixeira icon" )}
            Spacer(modifier = Modifier.width(5.dp))

            Button(onClick = {
                navController.navigate("Contato")
            },
               modifier = Modifier.fillMaxWidth()
                ) { Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.direita),
                contentDescription ="Seta direita icon" )}
        }
    }
}


fun cepAPI (cep:String, callback: (String) -> Unit){
    val urll = "https://viacep.com.br/ws/$cep/json"
    CoroutineScope(Dispatchers.IO).launch {
        try {
            val json = withContext(Dispatchers.IO){
                URL(urll).readText()
            }
            val data = JSONObject(json)
            if ("erro" !in json){
                val uf = data.getString("uf")
                val cidade = data.getString("localidade")
                val bairro = data.getString("bairro")
                val rua = data.getString("logradouro")


                val full = "$uf;$cidade;$bairro;$rua"
                callback(full)


            }else{callback("Erro: CEP não encontrado")}

        } catch (e: Exception) {
            callback("Erro na busca do CEP: ${e.message}")
        }
    }
}