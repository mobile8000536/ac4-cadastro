package com.example.cadastro.front

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.isDigitsOnly
import androidx.navigation.NavController
import com.example.cadastro.R
import com.example.cadastro.back.Validator

@OptIn(ExperimentalMaterial3Api::class)
@Composable

fun DadosPessoais(navController: NavController){
    //Variáveis de dados
    var nome by remember { mutableStateOf("") }
    var idade by remember { mutableStateOf("") }
    val context = LocalContext.current



    Column (
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.8f)
            .padding(top = 40.dp, start = 15.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top)
    {
        //Título
        Row(Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center)

        {
            Image(painter = painterResource(id = R.drawable.perfil),
                modifier = Modifier.width(50.dp) ,
                contentDescription = "Verify Icon" )
            Spacer(modifier = Modifier.width(20.dp))

            Text(text = "Dados Pessoais",
                fontWeight = FontWeight.W900,
                fontSize = 26.sp)
        }
        Spacer(modifier = Modifier.height(200.dp))

        //Bloco de Inputs
        Row{
            Image(painter = painterResource(id = R.drawable.check),
                contentDescription = "Perfil Icon",
                modifier = Modifier.width(50.dp) )
            Spacer(modifier = Modifier.width(5.dp))
            TextField(value = nome ,
                onValueChange ={nome = it},
                label = { Text(text = "Nome")},
                singleLine = true)
        }

        Spacer(modifier = Modifier.height(20.dp))

        Row{
            Image(painter = painterResource(id = R.drawable.balao),
                contentDescription = "Perfil Icon",
                modifier = Modifier.width(50.dp) )

            Spacer(modifier = Modifier.width(5.dp))
            TextField(value = idade ,
                onValueChange ={if(it.isDigitsOnly()){idade = it}},
                modifier = Modifier.width(100.dp),
                label = { Text(text = "Idade")},
                singleLine = true,
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number))

        }
    }

    //Bloco de botões
    Column(
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(1f)
            .padding(horizontal = 15.dp, vertical = 20.dp),
        verticalArrangement = Arrangement.Bottom) {

        Row (Modifier.fillMaxWidth()){
            Button(onClick = {
                nome=""
                idade=""
            },
                modifier = Modifier.fillMaxWidth(0.5f)
            ){ Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.lixeira),
                contentDescription ="lixeira icon" )}
            Spacer(modifier = Modifier.width(20.dp))

            Button(onClick = {
                if(Validator(nome).nome() && idade.toInt()<120){
                    navController.navigate("Endereco")
                }else{
                    Toast.makeText(context,"Dados inválidos",Toast.LENGTH_SHORT).show()
                }
            },
                modifier = Modifier.fillMaxWidth(1f)
                ) { Image(modifier = Modifier.width(30.dp),
                painter = painterResource(id = R.drawable.direita),
                contentDescription ="Seta direita icon" )}
        }
    }
}