package com.example.cadastro

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.cadastro.front.Confirmacao
import com.example.cadastro.front.Contato
import com.example.cadastro.front.DadosPessoais
import com.example.cadastro.front.Endereco
import com.example.cadastro.ui.theme.CadastroTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CadastroTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()

                    NavHost(navController = navController, startDestination = "DadosPessoais") {
                        composable("DadosPessoais") { DadosPessoais(navController) }
                        composable("Endereco") { Endereco(navController) }
                        composable("Contato") { Contato(navController) }
                        composable(
                            "Confirmacao/{email}/{codigo}",
                            arguments = listOf(
                                navArgument("email") { type = NavType.StringType },
                                navArgument("codigo") { type = NavType.StringType })
                        ) {
                            val email = it.arguments?.getString("email")!!
                            val codigo = it.arguments?.getString("codigo")!!
                            Confirmacao(navController, email, codigo)
                        }
                    }
                }
            }
        }
    }
}