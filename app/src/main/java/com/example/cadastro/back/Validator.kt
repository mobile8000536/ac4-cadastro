package com.example.cadastro.back

class Validator (testString:String){

    private val testString:String

    private val ddds = listOf(
        11,12,13,14,15,16,17,18,19,21,22,24,27,28,31,32,33,34,35,37,38,
        41,42,43,44,45,46,47,48,49,51,52,53,54,55,61,62,63,64,65,66,67,68,69,
        71,73,74,75,77,79,81,82,83,84,85,86,87,88,89,91,92,93,94,95,96,97,98,99
    )



    init {
        this.testString = testString
    }

    fun email(): Boolean{
        if(testString.isBlank() || '@' !in testString || testString.count{it == '@'}>1 || '.' !in testString) {return false}

        val split1 = this.testString.split("@")
        val split2 = split1[1].split(".")
        return (
                (split1.size==2 && (split1[0]!="" && split1[1]!=""))        //teste: xx@xx
                        && (split2.size==2 && (split2[0]!="" && split2[1]!=""))   //teste: xxx.xxx
                        //TODO - teste sem caracteres especiais após @
                )
    }

    fun telefone(): Boolean{
        if(testString.isBlank())return false

        return (testString[2]=='9')       //teste: primeiro numero '9'
                &&(testString.substring(0,2).toInt() in ddds)      //teste: ddd válido
                &&(testString.length ==11)       //teste: exatos 11 digitos
    }

    fun nome(): Boolean{
        if (" " !in testString){return false}
        var split = testString.split(" ")

        //retorna falso para: em branco || menor que 3 || ( nao contem caractere antes&depois do espaco)
        return !(testString.isBlank() || testString.replace(" ","").length<3 || (split[0]== "" || split[1]==""))
    }

    fun code(truecode:String):Boolean{
        return testString==truecode
    }
}