package com.example.cadastro.back

import com.sendgrid.helpers.mail.objects.Content
import com.sendgrid.helpers.mail.objects.Email


fun sendMail(eMail:String):String{
    val codigo = gerarCodigo()
    val from = Email("")
    val subject = "Validar Email"
    val to = Email("")
    val content = Content("text/html","<H1>Código de verificação:</H1><H2>$codigo</H2>")
    return  codigo
}

fun gerarCodigo():String{
    val range = 0..9
    var codigo=""
    for (i in 1..6){
        codigo+= range.random().toString()
    }
    return  codigo
}